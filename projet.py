import csv
import sqlite3 as sql3
from sqlite3 import Error
def connexion_fichier(Nom_fichier):
    """
    Permet de connecter a une base de donnée
    """
    conn = None
    try:
        conn = sql3.connect(Nom_fichier)
        print(sql3.version)
    except Error as e:
        print(e)
    return conn
def copie_bd_fichier(ma_liste,chemin_fichier,annee):
    """
    permet de copier les lignes du fichier csv ou la vente c'est faite en isere et de rajouter l'année de la transaction à la liste
    """
    with open(chemin_fichier, 'r') as fichier:
        lecteur=csv.reader(fichier,delimiter='|')
        liste_float=[10,25,27,29,31,33,42]#10 = valeur fonciere , 25 = surface carrée lot 1, 27 = sc lot 2,29 = sc 3, 31=sc4, 33=sc5, 42 = surface terrain
        liste_int=[7,34,35,37,38,11]#
        for ligne in lecteur:
            if ligne[18]=='38' and ligne[10]!='':#enleve les données hors de l'isere et dont la valeur fonciere est nul 
                for n in range(0,len(ligne)):
                    if n in liste_float and ligne[n]!='':                        
                        ligne[n]=float(ligne[n].replace(',','.'))
                    elif n in liste_int and ligne[n]!='':                        
                        ligne[n]=int(ligne[n])
                ligne.append(int(annee))
                ma_liste.append(ligne)
def insert_immobilier(conn, transaction):
    """
    Ajoute une nouvelle transaction immobiliere à la base de donnée connecter avec conn
    transaction est une liste contenant les différentes données de la transaction
    """ 
    nb_colonne='?,'*43
    sql = f''' INSERT INTO immobilier (Code_service_CH,Reference_document,Articles_CGI_un,Articles_CGI_deux,Articles_CGI_trois,Articles_CGI_quatre,Articles_CGI_cinq,No_disposition,Date_mutation,Nature_mutation,Valeur_fonciere,No_voie,B_T_Q,Type_de_voie,Code_voie,Voie,Code_postal,Commune,Code_departement,Code_commune,Prefixe_de_section,Section,No_plan,No_Volume,lot_1,Surface_Carrez_du_1_lot,lot_2,Surface_Carrez_du_2_lot,lot_3,Surface_Carrez_du_3_lot,lot_4,Surface_Carrez_du_4_lot,lot_5,Surface_Carrez_du_5_lot,Nombre_de_lots,Code_type_local,Type_local,Identifiant_local,Surface_reelle_bati,Nombre_pieces_principales,Nature_culture,Nature_culture_speciale,Surface_terrain,Annee)
        VALUES({nb_colonne}?);'''
    cur = conn.cursor()
    cur.execute(sql,transaction)
    conn.commit()
def init_data(connex):
    '''
    crée une base de données et sa table et Permet d'extraire des fichiers csv (dvf) les valeurs foncieres d'isere et de les inserer sur la base de données nouvellement crée
    '''
    create_table_sq = """ CREATE TABLE IF NOT EXISTS immobilier (id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
        Code_service_CH varchar(20),
        Reference_document varchar(20),
        Articles_CGI_un varchar(20),
        Articles_CGI_deux varchar(20),
        Articles_CGI_trois varchar(20),
        Articles_CGI_quatre varchar(20),
        Articles_CGI_cinq varchar(20),
        No_disposition INTEGER,
        Date_mutation date,
        Nature_mutation varchar(20),
        Valeur_fonciere float,
        No_voie INTEGER,
        B_T_Q varchar(20),
        Type_de_voie varchar(20),
        Code_voie varchar(20),
        Voie varchar(20),
        Code_postal varchar(20),
        Commune varchar(20),
        Code_departement varchar(20),
        Code_commune varchar(20),
        Prefixe_de_section varchar(20),
        Section varchar(20),
        No_plan varchar(20),
        No_Volume varchar(20),
        lot_1 varchar(20),
        Surface_Carrez_du_1_lot float,
        lot_2 varchar(20),
        Surface_Carrez_du_2_lot float,
        lot_3 varchar(20),
        Surface_Carrez_du_3_lot float,
        lot_4 varchar(20),
        Surface_Carrez_du_4_lot float,
        lot_5 varchar(20),
        Surface_Carrez_du_5_lot float,
        Nombre_de_lots INTEGER,
        Code_type_local INTEGER,
        Type_local varchar(20),
        Identifiant_local varchar(20),
        Surface_reelle_bati INTEGER,
        Nombre_pieces_principales INTEGER,
        Nature_culture varchar(20),
        Nature_culture_speciale varchar(20),
        Surface_terrain float,
        Annee INTEGER);"""
    ma_liste_immobilier=[]
    create_table(connex,create_table_sq)
    copie_tout_fichier_liste(connex,ma_liste_immobilier)
    for n in range(0,len(ma_liste_immobilier)):# Permet d'inserer toute les données iséroise dans notre base de données
        insert_immobilier(connex,ma_liste_immobilier[n])
    connex.commit()
def create_table(conn,create_table_sql):
    """ 
    Crée une table sur la base de données connecter avec conn
    create_table_sql est une requete sql permettant de crée une table et servant de parametre pour la fonction
    """
    try:
        c = conn.cursor()
        c.execute(create_table_sql)
        print('ok')
    except Error as e:
        print(e)
def supprimer_table(conn):
    """
    supprime la table immobilier
    """
    sql= '''DROP TABLE immobilier'''
    cur = conn.cursor()
    cur.execute(sql)
    conn.commit()
def prix_moyen_ville(conn,annee=0):
    '''
    fonction permettant d'afficher le prix moyen des valeurs foncieres par ville
    '''
    ville=str(input('Quel ville voulez-vous voir la valeur moyenne ?')).upper()
    cur = conn.cursor()
    if annee==0:        
        cur.execute(f"SELECT commune,ROUND(AVG(Valeur_fonciere),2) FROM immobilier WHERE Commune = '{ville}'")
        rows = cur.fetchall()
        for row in rows:
            print(f"la valeur moyen foncier pour la ville {row[0]} est de : {row[1]} €")
    else :
        boucle=True
        while boucle==True:
            continuer=str(input('Voulez vous rajouter un un type de bien et de vente ? (oui ou non)')).lower()
            if continuer=='non' or continuer=='n':
                cur.execute(f"SELECT Commune,ROUND(AVG(Valeur_fonciere),2),Annee FROM immobilier WHERE Commune = '{ville}' AND Annee={annee}")
                rows = cur.fetchall()
                for row in rows:
                    print(f"la valeur moyen foncier pour la ville {row[0]} est de : {row[1]} € pour l'année {row[2]}")
                boucle=False
            elif continuer=='oui' or continuer=='o':
                prix_type_vent(conn,ville,annee)
                boucle=False
            else:
                print('Vous devez rentrez oui ou non')
def select_test(conn):
    '''
    fonction permettant d'afficher le prix moyen des valeurs foncieres par ville
    '''
    cur = conn.cursor()
    cur.execute("select Type_local, Nature_mutation from immobilier GROUP BY Nature_mutation")
    #cur.execute("select Valeur_fonciere from immobilier")
    rows = cur.fetchall()
    for row in rows:
        print(row)
        #print(f"la valeur moyen foncier pour la ville {row[18]} est de :{row[11]}")
def choix_utilisateur(connex):
    '''
    fonction créant une interface utilisateur lui demandez quel action il compte faire
    '''
    continuer= True
    while continuer==True :
        try:
            choix= int(input('Quel recherche voulez vous faire ? tappez : \n 1 pour la moyenne des valeurs fonciere par ville \n 2 pour la moyenne par années \n 3 pour la moyenne par type de vente \n 4 pour une recherche avancé a partir une année \n 5 pour quitter : \n'))
        except:
            print('vous devez ecrire un nombre')
            continue        
        if choix==1 :
            prix_moyen_ville(connex)
        elif choix==2 :
            prix_moyen_annee(connex,initial=True)
        elif choix==3 :
            prix_type_vent(connex)
        elif choix==4:
            prix_moyen_annee(connex)
        elif choix==5 :
            continuer=False
        else :
            print("Vous devez entre une valeur comprise entre 1 et 4")
def prix_type_vent(conn,ville='',annee=0):
    '''
    fonction renvoyant les valeurs fonciere moyennes par type de ventes
    '''   
        
    boucle= True
    while boucle== True:
        try:
            type_bien=int(input("Veuillez choisir le type de bien (\n 1 pour maison,\n 2 pour appartement,\n 3 pour dependance,\n 4 pour un local industriel, commercial ou assimiler : \n"))
        except: 
            print("vous devez rentrez un chiffre")
            continue
        if 0<type_bien<=4 :
            try:
                type_transaction=int(input("Veuillez choisir le type de transaction \n1 pour vente \n 2 pour echange \n 3 pour Vente de terrain a batir \n 4 pour une vente en l'état pour de futur achevement\n 5 pour une Adjudication : \n"))
            except: 
                print("vous devez rentrez un chiffre")
            if 0<type_transaction<=5 :
                if type_transaction==1:
                    transaction="Vente"
                elif type_transaction==2:
                    transaction="Echange"
                elif type_transaction==3:
                    transaction="Vente terrain à bâtir"
                elif type_transaction==4:
                    transaction="Vente en l\'état futur d'achèvement"
                elif type_transaction==5:
                    transaction="Adjudication"
                if type_bien==1:
                    bien="Maison"
                elif type_bien==2:
                    bien="Appartement"
                elif type_bien==3:
                    bien="Dépendance"
                elif type_bien==4:
                    bien="Local industriel. commercial ou assimilé"
            else:
                print("le chiffre doit etre compris entre 0 et 5")
            cur = conn.cursor()
            if annee==0 and ville=='':   
                cur.execute(f'SELECT Type_local,Nature_mutation,round(AVG(Valeur_fonciere),2) FROM immobilier WHERE Type_local="{bien}" and Nature_mutation="{transaction}"')
                row = cur.fetchone()
                print(f"la valeur moyenne pour un(e) {row[0]} en {row[1]} est de : {row[2]} €")
            else:
                cur.execute(f'SELECT Type_local,Nature_mutation,round(AVG(Valeur_fonciere),2),Annee,Commune FROM immobilier WHERE Type_local="{bien}" and Nature_mutation="{transaction}" and Commune="{ville}" and Annee={annee}')
                row = cur.fetchone()                
                if row[0]!=None:
                    print(row)
                    print(f"la valeur moyenne pour un(e) {row[0]} en {row[1]} est de : {row[2]} € pour la ville de {row[4]} en {row[3]}")
                else:
                    print(f"Il n'y a pas ce genre de transaction pour la ville {ville} en {annee}")   
            boucle=False    
def copie_tout_fichier_liste(conn,ma_liste_2):
    '''
    fonction permettant de modifier ma_liste_2 en lui ajoutant a chaque index une liste contentant l'extraction de toute les données des transaction foncieres en isere.
    '''
    path=r'/home/alexis/csv/'
    fichiers= [f'{path}valeursfoncieres-2019.txt',f'{path}valeursfoncieres-2018.txt',f'{path}valeursfoncieres-2017.txt',f'{path}valeursfoncieres-2016.txt',f'{path}valeursfoncieres-2015.txt']       
    annee_fichier=2019
    copie_bd_fichier(ma_liste_2,fichiers[0],'2019')
    '''
    for fichier in fichiers:
        copie_bd_fichier(ma_liste_2,fichier,str(annee_fichier))
        annee_fichier-=1
    '''
def prix_moyen_annee(conn,initial=False):   
    boucle=True
    while boucle==True:
        try: 
            annee_choisi= int(input("ecrivez l'année dont vous souhaitez voir la moyenne : (Annee comprise en 2015 et 2019) :\n"))
        except: 
            print("vous devez rentrez une année")
            continue
        if (2015<=annee_choisi<=2019):
            boucle_ville=True
            if initial==False:
                '''
                while boucle_ville==True:
                    choix=str(input("Voulez rechercher pour une ville precise ?(o ou n)")).lower()
                    if choix=='o'or choix=='oui':
                '''
                prix_moyen_ville(conn,annee_choisi)        
                '''        boucle_ville=False
                    elif choix=='n' or choix=='non':
                        cur = conn.cursor()
                        cur.execute(f"SELECT Annee,round(AVG(Valeur_fonciere),1) FROM immobilier WHERE Annee= {annee_choisi}")
                        rows = cur.fetchall()
                        for row in rows:
                            print(f"Pour l'année {row[0]} la valeur moyenne est de : {row[1]} €")
                        boucle_ville=False
                    else:
                        print('Vous devez rentrez oui ou non')
                    boucle=False
                '''
            else:
                cur = conn.cursor()
                cur.execute(f"SELECT Annee,round(AVG(Valeur_fonciere),1) FROM immobilier WHERE Annee= {annee_choisi}")
                rows = cur.fetchall()
                for row in rows:
                    print(f"Pour l'année {row[0]} la valeur moyenne est de : {row[1]} €")
                boucle=False
        else:
            print("l'années doit etre comprise en 2015 et 2019")
def main():   
    nom_fichier = r"/home/alexis/git/immobilier/immobilier4.db"    
    connex=connexion_fichier(nom_fichier)
    #select_test(connex)
    #init_data(connex)
    choix_utilisateur(connex)
if __name__ == "__main__":
    main()